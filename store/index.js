import customLoading from './modules/custom-loading'
import authenticated from './modules/authenticated'
import menu from './modules/menu'
import testData from './modules/test-data'
import authTest from './modules/auth-test'
import product from './modules/product'
import productOrder from './modules/product-order'
import sellProduct from './modules/sell-product'
import board from './modules/board'
import basket from './modules/basket'

export const state = () => ({})

export const mutations = {}

export const modules = {
    customLoading,
    authenticated,
    menu,
    testData,
    authTest,
    product,
    productOrder,
    sellProduct,
    board,
    basket
}
