const BASE_URL = '/v1/product-order'

export default {
    DO_LIST: `${BASE_URL}/list`, //get
    DO_CREATE: `${BASE_URL}/data`
}
