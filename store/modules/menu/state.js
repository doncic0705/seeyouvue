const state = () => ({
    globalMenu: [
        {
            parentName: 'HOME',
            menuLabel: [
                { id: 'DASH_BOARD', icon: 'el-icon-tickets', currentName: '대시보드', link: '/', isShow: true },
            ]
        },
        {
            parentName: '회원관리',
            menuLabel: [
                { id: 'MEMBER_ADD', icon: 'el-icon-tickets', currentName: '회원등록', link: '/', isShow: false },
                { id: 'MEMBER_EDIT', icon: 'el-icon-tickets', currentName: '회원수정', link: '/', isShow: false },
                { id: 'MEMBER_LIST', icon: 'el-icon-tickets', currentName: '회원리스트', link: '/customer/list', isShow: true },
                { id: 'MEMBER_DETAIL', icon: 'el-icon-tickets', currentName: '회원상세정보', link: '/', isShow: false },
            ]
        },
        {
            parentName: '상품관리',
            menuLabel: [
                { id: 'PRODUCT_ADD', icon: 'el-icon-tickets', currentName: '상품등록', link: '/product/form', isShow: true },
                { id: 'PRODUCT_EDIT', icon: 'el-icon-tickets', currentName: '상품수정', link: '/product/edit', isShow: true },
                { id: 'PRODUCT_LIST', icon: 'el-icon-tickets', currentName: '상품리스트', link: '/product/list', isShow: true },
            ]
        },
        {
            parentName: '판매상품관리',
            menuLabel: [
                { id: 'SELL_PRODUCT_ADD', icon: 'el-icon-tickets', currentName: '판매상품등록', link: '/sell-product/form', isShow: true },
                { id: 'SELL_PRODUCT_EDIT', icon: 'el-icon-tickets', currentName: '판매상품수정', link: '/sell-product/edit', isShow: true },
                { id: 'SELL_PRODUCT_LIST', icon: 'el-icon-tickets', currentName: '판매상품리스트', link: '/sell-product/list', isShow: true },
            ]
        },
        {
            parentName: '발주관리',
            menuLabel: [
                { id: 'PRODUCT_ORDER_ADD', icon: 'el-icon-tickets', currentName: '발주등록', link: '/product-order/form', isShow: true },
                { id: 'PRODUCT_ORDER_EDIT', icon: 'el-icon-tickets', currentName: '발주수정', link: '/product-order/edit', isShow: true },
                { id: 'PRODUCT_ORDER_LIST', icon: 'el-icon-tickets', currentName: '발주리스트', link: '/product-order/list', isShow: true },
            ]
        },
        {
            parentName: '게시판 관리',
            menuLabel: [
                {id: 'BOARD_ADD', icon: 'el-icon-tickets', currentName: '게시글 등록', link: '/board/add', isShow: true},
                {id: 'BOARD_LIST', icon: 'el-icon-tickets', currentName: '게시글 리스트', link: '/board/list', isShow: true},
            ]
        },
        {
            parentName: '장바구니 관리',
            menuLabel: [
                {id: 'BASKET_ADD', icon: 'el-icon-tickets', currentName: '장바구니 등록', link: '/basket/form', isShow: true},
                {id: 'BASKET_LIST', icon: 'el-icon-tickets', currentName: '장바구니 리스트', link: '/basket/list', isShow: true},
            ]
        },
        {
            parentName: '마이 메뉴',
            menuLabel: [
                { id: 'MEMBER_LOGOUT', icon: 'el-icon-lock', currentName: '로그아웃', link: '/my-menu/logout', isShow: true },
            ]
        },
    ],
    selectedMenu: 'DASH_BOARD'
})

export default state
