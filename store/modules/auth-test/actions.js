import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_TEST_DATA]: (store) => {
        return axios.get(apiUrls.DO_TEST_DATA)
    },
}
