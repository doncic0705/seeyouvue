const BASE_URL = '/v1/basket'

export default {
    DO_LIST: `${BASE_URL}/product-id/list`, //get
    DO_CREATE: `${BASE_URL}/product-id/{productId}`
}
