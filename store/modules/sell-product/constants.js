export default {
    DO_SELL_PRODUCT_DATA: 'sell-product/doSellProductData',
    DO_CREATE: 'sell-product/doCreate',
    DO_DETAIL: 'sell-product/doDetail',
    DO_UPDATE: 'sell-product/doUpdate'
}
