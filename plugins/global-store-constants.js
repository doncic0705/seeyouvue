import Vue from 'vue'

import customLoadingConstants from '~/store/modules/custom-loading/constants'
Vue.prototype.$customLoadingConstants = customLoadingConstants

import authenticatedConstants from '~/store/modules/authenticated/constants'
Vue.prototype.$authenticatedConstants = authenticatedConstants

import menuConstants from '~/store/modules/menu/constants'
Vue.prototype.$menuConstants = menuConstants

import testDataConstants from '~/store/modules/test-data/constants'
Vue.prototype.$testDataConstants = testDataConstants

import authTestConstants from '~/store/modules/auth-test/constants'
Vue.prototype.$authTestConstants = authTestConstants

import productConstants from '~/store/modules/product/constants'
Vue.prototype.$productConstants = productConstants

import productOrderConstants from '~/store/modules/product-order/constants'
Vue.prototype.$productOrderConstants = productOrderConstants

import sellProductConstants from '~/store/modules/sell-product/constants'
Vue.prototype.$sellProductConstants = sellProductConstants

import boardConstants from '~/store/modules/board/constants'
Vue.prototype.$boardConstants = boardConstants

import basketConstants from '~/store/modules/basket/constants'
Vue.prototype.$basketConstants = basketConstants
